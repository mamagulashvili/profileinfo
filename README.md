### News App ###

#### Goal ####

* This app is created to make it easy for users to get the latest European news

#### How to use ####

1. Sign up and Log in

2. Open News Page

3. Click on article and open article in web

4. If you want click the save button and read the article later

5. Delete saved articles with a swipe

#### Used ####

* HTTP Request -> Retrofit

* Database -> Room Sql Lite, Firebase Firestore

* UI -> RecycleView, ViewPager, TabLayout, Navigation, Fragment

* API -> newsapi.org

#### Screenshots ####

![image](https://s2.im.ge/2021/06/13/QmgoT.png)

![image](https://s2.im.ge/2021/06/13/QmYjW.png)

![image](https://s2.im.ge/2021/06/13/QmbW0.png)
