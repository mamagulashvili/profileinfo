package com.example.tbcandroidinfopage

import android.content.SharedPreferences
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.tbcandroidinfopage.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity(), View.OnLongClickListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPreferences = getSharedPreferences("infoData", MODE_PRIVATE)

        backgroundAnimation()
        validateUserInputs()

        binding.saveButton.setOnClickListener {
            save()
        }
        binding.clearButton.setOnLongClickListener(this)
    }

    private fun save() {
        val email = binding.emailET.text.toString().trim()
        val userName = binding.userNameET.text.toString()
        val firstName = binding.firstNameET.text.toString()
        val lastName = binding.firstNameET.text.toString()
        val age = binding.ageET.text.toString()

        if (email.isNotEmpty() && userName.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            if (!isValidEmail(email)) {
                Snackbar.make(
                    binding.constraintContainer,
                    "invalid Email Address",
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                if (userName.length < 10) {
                    Snackbar.make(
                        binding.constraintContainer,
                        "User Name is too short",
                        Snackbar.LENGTH_SHORT
                    ).show()
                } else {
                    val editor = sharedPreferences.edit()
                    editor.apply {
                        putString("email", email)
                        putString("user_name", userName)
                        putString("first_name", firstName)
                        putString("last_name", lastName)
                        putString("age", age)
                    }
                    editor.apply()
                    Snackbar.make(binding.constraintContainer, "Saved", Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        } else {
            Toast.makeText(this, "Please Fill All Fields!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun validateUserInputs() {
        binding.emailET.doOnTextChanged { text, _, _, _ ->
            if (!text?.let { isValidEmail(it) }!!) {
                binding.emailContainer.error = "Incorrect Email "
            } else {
                binding.emailContainer.error = null
            }
        }
        binding.userNameET.doOnTextChanged { text, _, _, _ ->
            if (text!!.length > 10) {
                binding.userNameContainer.error = null
            } else {
                binding.userNameContainer.error = "User Name Must be More than 10 Char"
            }
        }
        binding.firstNameET.doOnTextChanged { text, _, _, _ ->
            if (text!!.isEmpty()){
                binding.firstNameContainer.error = "Please Enter Your First Name"
            }else
                binding.firstNameContainer.error = null
        }
        binding.lastNameET.doOnTextChanged { text, _, _, _ ->
            if (text!!.isEmpty()){
                binding.lastNameContainer.error = "Please Enter Your Last Name"
            }else
                binding.lastNameContainer.error = null
        }
        binding.ageET.doOnTextChanged { text, _, _, _ ->
            if (text!!.isEmpty()){
                binding.ageContainer.error = "Please Enter Your Age"
            }else
                binding.ageContainer.error = null
        }

    }

    override fun onLongClick(v: View?): Boolean {
        var isCleared = true

        binding.emailET.setText("")
        binding.userNameET.setText("")
        binding.lastNameET.setText("")
        binding.firstNameET.setText("")
        binding.ageET.setText("")

        val email = sharedPreferences.getString("email", "")
        val userName = sharedPreferences.getString("user_name", "")
        val lastName = sharedPreferences.getString("last_name", "")
        val age = sharedPreferences.getString("age", "")
        val firstName = sharedPreferences.getString("first_name", "")


        Snackbar.make(
            binding.constraintContainer,
            "All fields are deleted ",
            Snackbar.LENGTH_SHORT,
        ).apply {
            setAction("Undo") {
                binding.emailET.setText(email)
                binding.userNameET.setText(userName)
                binding.lastNameET.setText(lastName)
                binding.firstNameET.setText(firstName)
                binding.ageET.setText(age)
                isCleared = false
            }.show()
        }
        if (isCleared) {
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
        }
        return true
    }

    private fun backgroundAnimation() {
        val drawableAnim: AnimationDrawable =
            binding.constraintContainer.background as AnimationDrawable
        drawableAnim.apply {
            setEnterFadeDuration(1200)
            setExitFadeDuration(3000)
            start()
        }
    }
}